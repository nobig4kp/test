import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class KTest {

    public WebDriver driver;
    //Declare a URL
    public String testURL = "http://www.overstock.com/";

    //-----------------------------------Test Setup-----------------------------------
    @BeforeMethod
    public void setupTest (){
        //Create a ChromeDriver
        driver = new ChromeDriver();
        driver.navigate().to(testURL);
    }

    //-----------------------------------Tests-----------------------------------
    @Test
    public void homeTest () {
        //page title
        String title = driver.getTitle();

        //page title
        System.out.println("Page Title: " + title);

        //Assert
        Assert.assertEquals(title, "Overstock: The Best Deals Online : Furniture, Bedding, Jewelry & More", "Expected Overstock. Found: "+title);
    }
    @Test
    public void beddingTest () {
        //Get page title
        WebElement testAutomationLink = driver.findElement(By.xpath("//*[@id=\"hd1TopNav\"]/a[4]"));
        //click link
        testAutomationLink.click();
        String title = driver.getTitle();

        //Print page's title
        System.out.println("Page Title: " + title);

        //Assertion"Test Automation
        Assert.assertEquals(title, "Shop Bedding & Bath | Discover our Best Deals at Overstock.com", "Title assertion is failed!");
    }

    @Test
    public void bathTest () {
        //Get page title
        WebElement testAutomationLink = driver.findElement(By.xpath("//*[@id=\"nl1-leftnav-li\"]/a[1]"));
        //click link
        testAutomationLink.click();
        String title = driver.getTitle();

        //Print page's title
        System.out.println("Page Title: " + title);

        //Assertion"Test Automation
        Assert.assertEquals(title, "Bath & Towels | Shop our Best Bedding & Bath Deals Online at Overstock.com", "Title assertion is failed!");
    }

    //-----------------------------------Test TearDown-----------------------------------
    @AfterMethod
    public void teardownTest (){
        //Close browser and end the session
        driver.quit();
    }
}
